# What is this?
Tools used to describe an Android application. It possesses translation mechanisms to recognize smali directives and class structures to hold most java elements (Classes, Methods, Invocations, Attributes..) and android elements (activities, manifests, etc). It also possesses tools for graphically representing this structures and to export them as json files.

# How to use?

## From json to SmaliApkRepresentation

	File[] files = new File("apks_path/").listFiles();

	for (File file : files) {
		SmaliApkRepresentation apk = SmaliApkExporter.loadJSON(file);
		System.out.println("Loaded Json " + apk.getApkName());
	}

## Iterating though method invocations

		for(SmaliClassRepresentation cls : apk.getClasses()){
		    System.out.println("Class name : " + cls.getClassName());
		    for(SmaliMethodRepresentation mth : cls.getMethods()){
		        System.out.println("-Method name : " + mth.getMethodName());
		        for(SmaliLinkRepresentation lnk : mth.getLinks()){
		            System.out.println("--Link name : " + lnk.getLinkName());
		        }
		    }
		} 
## Getting application manifest

	apk.getManifest();
	
## Getting the naming table (translates IDS to names)

	apk.getNameEncoding();