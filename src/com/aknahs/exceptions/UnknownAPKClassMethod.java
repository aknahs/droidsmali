package com.aknahs.exceptions;

public class UnknownAPKClassMethod extends SmaliApkException{

	private static final long serialVersionUID = -9159967658443138433L;

	public UnknownAPKClassMethod(String apk, String cls, String mth) {
		super("Could not find method " + mth + " in class " + cls + " in apk " + apk);
	}

}
