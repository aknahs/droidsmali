package com.aknahs.exceptions;

public class UnknownAPKClass extends SmaliApkException{

	private static final long serialVersionUID = -245737211561737775L;

	public UnknownAPKClass(String apk, String cls) {
		super("Could not find class " + cls + " in apk " + apk);
	}

}
