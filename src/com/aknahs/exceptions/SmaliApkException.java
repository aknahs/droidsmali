package com.aknahs.exceptions;

public class SmaliApkException extends Exception{

	private static final long serialVersionUID = -8291336760885371226L;

	public SmaliApkException(String message){
		super(message);
	}
	
}
