package com.aknahs.manifest;

import java.util.ArrayList;

import com.aknahs.smali.representations.SmaliNameEncoding;
import com.google.gson.annotations.Expose;

public class ManifestRepresentation {

	@Expose public String _apkname = null;
	@Expose public String _mainActivity = null;
	@Expose public ArrayList<Integer> _receivers = new ArrayList<Integer>();
	@Expose public ArrayList<Integer> _activities = new ArrayList<Integer>();
	
	public SmaliNameEncoding _nameSchema = null;

	
	public ManifestRepresentation(SmaliNameEncoding nameSchema){
		_nameSchema = nameSchema;
	}
	
	public ManifestRepresentation(SmaliNameEncoding nameSchema, String apkname, String activity){
		_apkname = apkname;
		_mainActivity = activity;
		_nameSchema = nameSchema;
	}
	
	public void registerActivity(String activity){
		if(_nameSchema == null){
			return;
		}
		else{
			Integer id = _nameSchema.registerID(activity);
			_activities.add(id);
		}
	}
	
	public void registerReceiver(String receiver){
		if(_nameSchema == null){
			return;
		}
		else{
			Integer id = _nameSchema.registerID(receiver);
			_receivers.add(id);
		}
	}
	
	public ArrayList<Integer> getReceivers(){
		return _receivers;
	}
}
