package com.aknahs.manifest;

public class ManifestDirectives {

	public static enum DIRECTIVES {
		MANIFEST, ACTIVITY, ACTION, RECEIVER, UNKNOWN, ACTIVITY_ALIAS, PERMISSION, USES_FEATURE, USES_PERMISSION, APPLICATION, CATEGORY, DATA, GRANT_URI_PERMISSION, INSTRUMENTATION, INTENT_FILTER, META_DATA, PERMISSION_GROUP, PERMISSION_TREE, PROVIDER, SERVICE, SUPPORTS_SCREENS, USES_CONFIGURATION, USES_LIBRARY, USES_SDK
	}

	public static boolean isDirective(String dir) {
		if (dir != null && dir.length() > 0 && dir.charAt(0) == '<')
			return true;
		return false;
	}

	/*
	 * <action> <activity> <activity-alias> <application> <category> <data>
	 * <grant-uri-permission> <instrumentation> <intent-filter> <manifest>
	 * <meta-data> <permission> <permission-group> <permission-tree> <provider>
	 * <receiver> <service> <supports-screens> <uses-configuration>
	 * <uses-feature> <uses-library> <uses-permission> <uses-sdk>
	 */

	public static DIRECTIVES convertDirective(String dir) {
		switch (dir) {
		case "<grant-uri-permission":
			return DIRECTIVES.GRANT_URI_PERMISSION;
		case "<instrumentation":
			return DIRECTIVES.INSTRUMENTATION;
		case "<intent-filter":
			return DIRECTIVES.INTENT_FILTER;
		case "<meta-data":
			return DIRECTIVES.META_DATA;
		case "<permission-group":
			return DIRECTIVES.PERMISSION_GROUP;
		case "<permission-tree":
			return DIRECTIVES.PERMISSION_TREE;
		case "<provider":
			return DIRECTIVES.PROVIDER;
		case "<service":
			return DIRECTIVES.SERVICE;
		case "<supports-screens":
			return DIRECTIVES.SUPPORTS_SCREENS;
		case "<uses-configuration":
			return DIRECTIVES.USES_CONFIGURATION;
		case "<uses-library":
			return DIRECTIVES.USES_LIBRARY;
		case "<uses-sdk":
			return DIRECTIVES.USES_SDK;		
		case "<category":
			return DIRECTIVES.CATEGORY;
		case "<data":
			return DIRECTIVES.DATA;
		case "<manifest":
			return DIRECTIVES.MANIFEST;
		case "<activity":
			return DIRECTIVES.ACTIVITY;
		case "<activity-alias":
			return DIRECTIVES.ACTIVITY_ALIAS;
		case "<permission":
			return DIRECTIVES.PERMISSION;
		case "<uses-permission":
			return DIRECTIVES.USES_PERMISSION;
		case "<uses-feature":
			return DIRECTIVES.USES_FEATURE;
		case "<action":
			return DIRECTIVES.ACTION;
		case "<receiver":
			return DIRECTIVES.RECEIVER;
		case "<application":
			return DIRECTIVES.APPLICATION;

		default:
			return DIRECTIVES.UNKNOWN;
		}
	}
}
