package com.aknahs.graphviz;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Set;

import com.aknahs.log.SmaliLogger;

public class DotExporter {

	private String RESULTS_FOLDER = "results/";
	private String DOT_FOLDER = "/usr/bin/dot";
	private String SFDP_FOLDER = "/usr/bin/neato";
	// shape=record
	private String SFDP_NODE_STYLE = "shape=record,style=filled, fillcolor=cornflowerblue, fontcolor=white, fontsize=10";
	private String SFDP_GRAPH_STYLE = "overlap=false, splines=true, bgcolor=white";

	public static enum DotType {
		NORMAL, RECORD
	}

	public DotType _type = DotType.NORMAL;

	public int _edgesCounter = 0;

	// Generate differentiating edge styles "randomly" for each tag
	// it is static so that tag colors are the same among DotExporters!
	private static HashMap<String, DotEdgeStyle> _tags = new HashMap<String, DotEdgeStyle>();

	public static Set<String> getTags() {
		SmaliLogger.log("DotExporter","getTags() returned " + _tags.toString());
		return _tags.keySet();
	}

	private StringBuilder _graph = new StringBuilder();

	public DotEdgeStyle registerEdgeTag(String tag) {
		if (_tags.containsKey(tag))
			return _tags.get(tag);

		// DotEdgeStyle randomizes the color of the edge
		DotEdgeStyle style = new DotEdgeStyle();

		_tags.put(tag, style);

		return style;
	}

	public DotEdgeStyle registerEdgeTag(String tag, String color) {
		if (_tags.containsKey(tag))
			return _tags.get(tag);

		DotEdgeStyle style = new DotEdgeStyle(color);
		// DotEdgeStyle randomizes the color of the edge
		_tags.put(tag, style);

		return style;
	}

	public DotEdgeStyle registerEdgeTag(String tag, float h, float s, float v) {
		if (_tags.containsKey(tag))
			return _tags.get(tag);

		DotEdgeStyle style = new DotEdgeStyle(h, s, v);
		// DotEdgeStyle randomizes the color of the edge
		_tags.put(tag, style);

		return style;
	}

	public DotExporter(DotType type) {
		_type = type;
	}

	public DotExporter() {
		_type = DotType.NORMAL;
	}

	public String getDotSource() {
		return _graph.toString();
	}

	public static String dotifyString(String cmd) {
		String ret = "";

		String aux = cmd.replace('.', '_').replace('$', '_');

		if (aux.charAt(0) == '<')
			ret = aux.substring(1, aux.length() - 1);
		else
			ret = aux;

		return ret;
	}

	private void addDotLine(String line) {
		_graph.append(line + "\n");
	}

	public void addEdge(String clsName, String mthName, String clsName2,
			String mthName2) {
		addDotLine(dotifyString(clsName) + "_" + dotifyString(mthName) + " -> "
				+ dotifyString(clsName2) + "_" + dotifyString(mthName2) + "s;");
		_edgesCounter++;
	}

	public void addEdge(String clsName, String mthName, String clsName2,
			String mthName2, String label) {
		addDotLine(dotifyString(clsName) + "_" + dotifyString(mthName) + " -> "
				+ dotifyString(clsName2) + "_" + dotifyString(mthName2)
				+ "[color=white, penwidth=3, label=\"" + label + "\"];");
		_edgesCounter++;
	}

	
	public void addEdgeByTag(String clsName, String mthName, String clsName2,
			String mthName2, String tag, String label) {

		DotEdgeStyle style = registerEdgeTag(tag);

		addDotLine(dotifyString(clsName) + "_" + dotifyString(mthName) + " -> "
				+ dotifyString(clsName2) + "_" + dotifyString(mthName2) + "["
				+ style + ", label=\"" + label + "\"];");
		_edgesCounter++;
	}

	private String retrieveNodeStyle(String nodeName, Object[] stats) {
		int counter = 0;
		String nodeStyle = "\"{" + nodeName + "|{";

		for (Object s : stats) {
			if (counter > 0)
				nodeStyle += "|";
			nodeStyle += s.toString();
			counter++;
		}
		
		nodeStyle += "}}\"";
		
		return nodeStyle;
	}

	public void addEdgeByTag(String clsName, String mthName, String clsName2,
			String mthName2, String tag, String label, Object[] stats1,
			Object[] stats2) {

		DotEdgeStyle style = registerEdgeTag(tag);

		String node1 = dotifyString(clsName) + "_" + dotifyString(mthName);
		String node2 = dotifyString(clsName2) + "_" + dotifyString(mthName2);

		String nodeStyle1 = retrieveNodeStyle(node1, stats1);
		String nodeStyle2 = retrieveNodeStyle(node2, stats2);
		
		addDotLine(node1 + "[label=" + nodeStyle1 + "]");
		addDotLine(node2 + "[label=" + nodeStyle2 + "]");
		
		addDotLine(node1 + " -> " + node2 + "[" + style + ", label=\"" + label
				+ "\"];");
		_edgesCounter++;
	}

	public void addEdgeByTagNoMethod(String clsName, String mthName,
			String clsName2, String tag, String label) {

		DotEdgeStyle style = registerEdgeTag(tag);

		addDotLine(dotifyString(clsName) + "_" + dotifyString(mthName) + " -> "
				+ dotifyString(clsName2) + "[" + style + ", label=\"" + label
				+ "\"];");
		_edgesCounter++;
	}

	public void generateGraphImage(String name, String type) {

		String dotFile = RESULTS_FOLDER + name + ".dot";
		String outFile = RESULTS_FOLDER + name + "." + type;

		try {
			Runtime rt = Runtime.getRuntime();
			String[] args = new String[5];

			if (_type == DotType.NORMAL)
				args[0] = DOT_FOLDER;
			else
				args[0] = SFDP_FOLDER;

			args[1] = "-T" + type;
			args[2] = dotFile;
			args[3] = "-o";
			args[4] = outFile;

			Process p = rt.exec(args);

			p.waitFor();
		} catch (java.io.IOException e) {
			e.printStackTrace();
		} catch (java.lang.InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void saveDotFile(String name) {

		PrintWriter writer;

		try {
			SmaliLogger.log("DotExporter","Creating results/" + name + ".dot");
			writer = new PrintWriter(RESULTS_FOLDER + name + ".dot", "UTF-8");

			writer.println(_graph);
			writer.close();
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void startGraph() {
		if (_type == DotType.NORMAL)
			addDotLine("digraph G {");// \nsize=\"11,11\";\nlayout=neato;\noverlap=false;\nsplines=true;\npack=true;\nstart=\"random\";\nsep=0.1;";
		else
			addDotLine("digraph G {\n" + "graph [" + SFDP_GRAPH_STYLE + "];\n"
					+ "node [" + SFDP_NODE_STYLE + "];\n");
	}

	public void endGraph() {
		addDotLine("}");
	}

}
