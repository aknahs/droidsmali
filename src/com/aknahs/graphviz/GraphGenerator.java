package com.aknahs.graphviz;

import java.util.ArrayList;

import com.aknahs.graphviz.DotExporter.DotType;
import com.aknahs.log.SmaliLogger;
import com.aknahs.smali.representations.SmaliApkRepresentation;
import com.aknahs.smali.representations.SmaliClassRepresentation;
import com.aknahs.smali.representations.SmaliLinkRepresentation;
import com.aknahs.smali.representations.SmaliMethodRepresentation;

public class GraphGenerator {

	private static ArrayList<SmaliMethodRepresentation> _traversedMths = new ArrayList<SmaliMethodRepresentation>();
	public static boolean _autoPloting = false;

	// This should be called when the links have been reversed and the tags
	// propagated
	// it suffices to go around the apk without traversing and plotting known
	// nodes and
	// links that own the tag (i.e. no tagging propagation or anything)
	private static void addReverseLinksByTag(DotExporter[] exporters,
			SmaliApkRepresentation apk, String tag) {

		String auxTag = "";

		for (SmaliClassRepresentation cls : apk.getClasses()) {
			for (SmaliMethodRepresentation mth : cls.getMethods()) {

				if (tag != null) {
					if (!mth._accesses.containsTag(tag)) {
						continue;
					} else {
						auxTag = tag;
					}
				} else {
					if (mth._accesses.getTags().size() > 0)
						auxTag = mth._accesses.toString();
					else
						continue;
				}

				// Changed getLinks to getReverseLinks because they are the ones
				// used on propagation of tags!
				for (SmaliLinkRepresentation lnk : mth.getReverseLinks())
					addReverseLink(apk, cls, mth, lnk, exporters, auxTag);
			}
		}

		// For printing the honey pot classes and links.
		// although it might seem that it should already get caught in previous
		// cycles,
		// the honeyPots will not be present in the reverse links of other
		// methods
		// This is because android package classes have no knowledge of the
		// application classes :)
		for (SmaliClassRepresentation honeyCls : apk.getHoneyPots()
				.getSmaliKnownClasses()) {
			for (SmaliMethodRepresentation honeyMth : honeyCls.getMethods()) {

				if (tag != null) {
					if (!honeyMth._accesses.containsTag(tag)) {
						continue;
					} else {
						auxTag = tag;
					}
				} else {
					if (honeyMth._accesses.getTags().size() > 0)
						auxTag = honeyMth._accesses.toString();
					else
						continue;
				}

				for (SmaliLinkRepresentation honeyLnk : honeyMth
						.getReverseLinks())

					addReverseLink(apk, honeyCls, honeyMth, honeyLnk,
							exporters, auxTag);
			}
		}
	}

	public static void addReverseLink(SmaliApkRepresentation apk,
			SmaliClassRepresentation cls, SmaliMethodRepresentation mth,
			SmaliLinkRepresentation lnk, DotExporter[] exporters, String tag) {

		DotExporter gvGephi = exporters[0];
		DotExporter gvDot = exporters[1];

		String[] mth2stats = { "" + mth.getReverseLinks().size(),
				"" + mth.getLinks().size(), mth._accesses.getTagsString() };

		SmaliMethodRepresentation lnkmth;
		try {
			lnkmth = apk.getClass(lnk.getLinkClass()).getMethod(
					lnk.getLinkMethod());

			String[] mth1stats = { "" + lnkmth.getReverseLinks().size(),
					"" + lnkmth.getLinks().size(),
					lnkmth._accesses.getTagsString() };

			// Reverse order because its reverse links!
			gvDot.addEdgeByTag(apk.getNameEncoding()
					.getName(lnk.getLinkClass()), apk.getNameEncoding()
					.getName(lnk.getLinkMethod()), cls.getClassName(), mth
					.getMethodName(), tag, tag, (Object[]) mth1stats,
					(Object[]) mth2stats);

			// Reverse order because its reverse links!
			gvGephi.addEdge(apk.getNameEncoding().getName(lnk.getLinkClass()),
					apk.getNameEncoding().getName(lnk.getLinkMethod()),
					cls.getClassName(), mth.getMethodName(), tag);

		} catch (NullPointerException e) {
			// Probably a honeyPot

			lnkmth = apk.getHoneyPots().getSmaliClass(lnk.getLinkClass())
					.getMethod(lnk.getLinkMethod());

			if (lnkmth == null)
				lnkmth = new SmaliMethodRepresentation(apk.getNameEncoding(),
						apk.getNameEncoding().getName(lnk.getLinkClass()), apk
								.getNameEncoding().getName(lnk.getLinkMethod()));

			String[] mth1stats = { "" + lnkmth.getStaticOccurencies(),
					"" + lnkmth.getStaticOccurencies() };
			// Reverse order because its reverse links!
			gvDot.addEdgeByTag(apk.getNameEncoding()
					.getName(lnk.getLinkClass()), apk.getNameEncoding()
					.getName(lnk.getLinkMethod()), cls.getClassName(), mth
					.getMethodName(), tag, tag, (Object[]) mth1stats,
					(Object[]) mth2stats);
			// Reverse order because its reverse links!
			gvGephi.addEdge(apk.getNameEncoding().getName(lnk.getLinkClass()),
					apk.getNameEncoding().getName(lnk.getLinkMethod()),
					cls.getClassName(), mth.getMethodName(), tag);
		}
	}

	public static void generateTagGraph(SmaliApkRepresentation apk, String tag) {
		DotExporter[] exporters = startGraphs();
		addReverseLinksByTag(exporters, apk, tag);
		endGraphs(apk.getApkName() + "-tag-" + tag, exporters);
	}

	// Broken for gephi (doesnt allow parallel edges)
	/*
	 * public static void generateTagsGraph(SmaliApkRepresentation apk, String[]
	 * tags) {
	 * 
	 * DotExporter[] exporters = startGraphs();
	 * 
	 * String filename = "";
	 * 
	 * for (String tag : tags) { addReverseLinksByTag(exporters, apk, null);
	 * filename += "-" + tag; }
	 * 
	 * endGraphs(apk.getApkName() + filename, exporters); }
	 */

	// This is inefficient
	public static void generateTagsGraph(SmaliApkRepresentation apk) {

		DotExporter[] exporters = startGraphs();

		addReverseLinksByTag(exporters, apk, null);

		endGraphs(apk.getApkName() + "-AllTags", exporters);
	}

	private static DotExporter[] startGraphs() {
		DotExporter[] ret = { new DotExporter(), new DotExporter() };

		// Prepare a per-class graph (e.g. Socket reverse call graph)
		ret[0] = new DotExporter();
		ret[0].startGraph();

		// Dot graph
		ret[1] = new DotExporter(DotType.RECORD);
		ret[1].startGraph();

		return ret;
	}

	private static void endGraphs(String filename, DotExporter[] exporters) {
		// End dot graphs
		exporters[0].endGraph();
		exporters[1].endGraph();

		// Save .dot files
		exporters[0].saveDotFile(filename);
		exporters[1].saveDotFile(filename + "-GraphViz");

		// Export as png
		if (_autoPloting) {
			SmaliLogger.log("GraphGenerator","Generating graph with "
					+ exporters[1]._edgesCounter + "edges");
			if (exporters[1]._edgesCounter > 300) {
				System.out
						.println("Skipping generation of big graph (you can do it manually");
			} else
				exporters[1].generateGraphImage(filename + "-GraphViz", "png");
		}
	}

	public static void generateReverseGraphFromClass(
			SmaliApkRepresentation apk, SmaliClassRepresentation honeyCls) {

		SmaliLogger.log("GraphGenerator","Processing reverse honey class : "
				+ honeyCls.getClassName());

		DotExporter[] exporters = startGraphs();
		DotExporter gvGephi = exporters[0];
		DotExporter gvDot = exporters[1];

		// iterate over the honey class methods and respective links
		for (SmaliMethodRepresentation honeyMth : honeyCls.getMethods()) {
			SmaliLogger.log("GraphGenerator","Processing reverse honey method : "
					+ honeyMth.getMethodName());

			clearTraversedMethods();

			for (SmaliLinkRepresentation honeyReverseLink : honeyMth
					.getReverseLinks()) {

				SmaliLogger.log("GraphGenerator","Processing reverse honey links : "
						+ honeyReverseLink.getLinkClass() + "."
						+ honeyReverseLink.getLinkMethod() + " -> "
						+ honeyCls.getClassName() + "."
						+ honeyMth.getMethodName() + ";");

				SmaliClassRepresentation reverseCls = apk
						.getClass(honeyReverseLink.getLinkClass());

				if (reverseCls == null) {
					System.out
							.println("generateHoneyPotGraph - Missing reverseCls : "
									+ honeyReverseLink.getLinkClass());

				} else {
					SmaliMethodRepresentation reverseMth = reverseCls
							.getMethod(honeyReverseLink.getLinkMethod());

					String label = "" + honeyReverseLink.getStaticOccurencies();

					gvGephi.addEdge(
							apk.getNameEncoding().getName(
									honeyReverseLink.getLinkClass()),
							apk.getNameEncoding().getName(
									honeyReverseLink.getLinkMethod()),
							apk.getNameEncoding().getName(
									honeyMth.getOwnerClass()), honeyMth
									.getMethodName());// label);

					gvDot.addEdgeByTag(
							apk.getNameEncoding().getName(
									honeyReverseLink.getLinkClass()),
							apk.getNameEncoding().getName(
									honeyReverseLink.getLinkMethod()),
							apk.getNameEncoding().getName(
									honeyMth.getOwnerClass()), honeyMth
									.getMethodName(), "randomTag", label);

					// Ommitted the honey method to reduce the number of nodes!
					/*
					 * gvDot.addEdgeByTagNoMethod(honeyReverseLink.getLinkClass()
					 * , honeyReverseLink.getLinkMethod(),
					 * honeyMth.getOwnerClass(), "randomTag", label);
					 */
					recurseThroughLinks(apk, gvGephi, gvDot, reverseMth,
							"randomTag");

				}
			}
		}

		String filename = apk.getApkName() + "-" + honeyCls.getClassName();
		endGraphs(filename, exporters);
	}

	private static void recurseThroughLinks(SmaliApkRepresentation apk,
			DotExporter gephi, DotExporter dot,
			SmaliMethodRepresentation reverseMth, String tag) {

		if (reverseMth == null || reverseMth._traversed)
			return;
		else {
			reverseMth._traversed = true;
			_traversedMths.add(reverseMth);
		}

		for (SmaliLinkRepresentation link : reverseMth.getReverseLinks()) {
			gephi.addEdge(apk.getNameEncoding().getName(link.getLinkClass()),
					apk.getNameEncoding().getName(link.getLinkMethod()),
					apk.getNameEncoding().getName(reverseMth.getOwnerClass()),
					reverseMth.getMethodName(),
					"" + link.getStaticOccurencies());

			dot.addEdgeByTag(
					apk.getNameEncoding().getName(link.getLinkClass()), apk
							.getNameEncoding().getName(link.getLinkMethod()),
					apk.getNameEncoding().getName(reverseMth.getOwnerClass()),
					reverseMth.getMethodName(), tag,
					"" + link.getStaticOccurencies());

			recurseThroughLinks(
					apk,
					gephi,
					dot,
					apk.getClass(link.getLinkClass()).getMethod(
							link.getLinkMethod()), tag);
		}
	}

	public static void generateGephiGraph(SmaliApkRepresentation apk) {
		if (apk == null) {
			SmaliLogger.log("GraphGenerator","generateGraphizGraph : apk was null");
			return;
		}

		SmaliLogger.log("GraphGenerator","Exporting " + apk.getApkName());

		DotExporter gv = new DotExporter();
		gv.startGraph();

		for (SmaliClassRepresentation cls : apk.getClasses()) {
			for (SmaliMethodRepresentation mth : cls.getMethods()) {
				for (SmaliLinkRepresentation link : mth.getLinks()) {

					if (apk.getHashClasses().containsKey(link.getLinkClass())) {
						gv.addEdge(
								cls.getClassName(),
								mth.getMethodName(),
								apk.getNameEncoding().getName(
										link.getLinkClass()),
								apk.getNameEncoding().getName(
										link.getLinkMethod()),
								"" + link.getStaticOccurencies());
					}

				}
			}
		}

		gv.endGraph();
		gv.saveDotFile(apk.getApkName());
	}

	/*
	 * Method that traverses the honeyPot classes and generates a graph per
	 * class WARNING : It uses the SmaliRepresentation _traversed property to be
	 * able to find paths without looping!
	 */
	public static void generateReverseGraphPerClassForWholeAPK(
			SmaliApkRepresentation apk) {

		if (apk == null) {
			SmaliLogger.log("GraphGenerator","generateHoneyPotGraph : apk was null");
			return;
		}

		SmaliLogger.log("GraphGenerator","Exporting Graph for : " + apk.getApkName());

		for (SmaliClassRepresentation honeyCls : apk.getHoneyPots()
				.getSmaliKnownClasses()) {

			GraphGenerator.generateReverseGraphFromClass(apk, honeyCls);
		}
	}

	private static void clearTraversedMethods() {
		if (_traversedMths == null)
			return;

		for (SmaliMethodRepresentation cleanMth : _traversedMths)
			cleanMth._traversed = false;
		_traversedMths.clear();
	}
}
