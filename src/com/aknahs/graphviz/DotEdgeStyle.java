package com.aknahs.graphviz;

import java.util.Random;

public class DotEdgeStyle {
	public String _color = null;//"0.000 0.000 0.000"; //black
	public String _arrowHead = null;//"vee";
	public String _arrowTail = null;//"inv";
	public String _arrowSize = null;//"0.7";
	public String _fontSize = null;//"10";
	public String _fontColor = "black";//"white";
	public String _penWidth = "2";
	
	private static Random rand = new Random();
	
	public DotEdgeStyle(){
		_color = randomizeColor();
	}
	
	public DotEdgeStyle(String color){
		_color = color;
	}
	
	public DotEdgeStyle(float h, float s, float v){
		_color = ""+h+" "+s+" "+v;
	}
	
	private String randomizeColor(){
		// HSV is composed by 3 floats, from 0 to 1.
		float h = rand.nextFloat();
		float s = rand.nextFloat();
		float v = rand.nextFloat();
		
		//fixes to assure brigthness and color
		if(s<0.2)
			s+=0.2;
		if(v<0.2)
			v+=0.2;
		
		return "\""+h+" "+s+" "+v+"\"";
	}
	
	@Override
	public String toString(){
		String ret = "";
		int arguments = 0;
		
		if(_color != null){
			ret += "color=" + _color;
			arguments++;
		}
		
		if(_arrowHead != null){
			ret += (arguments>0?",":"") + "arrowhead=" + _arrowHead;
			arguments++;
		}
		
		if(_arrowTail != null){
			ret += (arguments>0?",":"") + "arrowtail=" + _arrowTail;
			arguments++;
		}
		
		if(_arrowSize != null){
			ret += (arguments>0?",":"") + "arrowsize=" + _arrowSize;
			arguments++;
		}
		
		if(_fontSize != null){
			ret += (arguments>0?",":"") + "fontsize=" + _fontSize;
			arguments++;
		}
		
		if(_fontColor != null){
			ret += (arguments>0?",":"") + "fontcolor=" + _fontColor;
			arguments++;
		}
		
		if(_penWidth != null){
			ret += (arguments>0?",":"") + "penwidth=" + _penWidth;
			arguments++;
		}
		
		
		return ret;
	}
}
