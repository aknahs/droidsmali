package com.aknahs.honeypots;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Set;

import com.aknahs.log.SmaliLogger;
import com.aknahs.smali.representations.SmaliNameEncoding;

public class PackageCategories {

	private static String _packagesPath = "./android.packages";
	private static String _ignoresPath = "./android.ignore";

	private static HashMap<String, ArrayList<String>> _tagsPerPackage = new HashMap<String, ArrayList<String>>();
	private static HashMap<String, ArrayList<String>> _packagesPerTag = new HashMap<String, ArrayList<String>>();
	private static ArrayList<String> _ignores = new ArrayList<String>();
	
	public static Set<String> getTags(){
		return _packagesPerTag.keySet();
	}
	
	public static void loadHoneyPotClasses(SmaliHoneyPots honeyPot) {

		// ---------------------------------SOCKETS-------------------------------------

		String[] mths = { "connect", "bind", "close" }; // "close"
		honeyPot.registerKnownClass("java.net.Socket", mths);

		String[] mthsSSL = { "connect", "bind", "getSession", "close" }; // "close"
		honeyPot.registerKnownClass("javax.net.SSLSocket", mthsSSL);

		String[] mthsSSLFactory = { "createSocket" }; // "close"
		honeyPot.registerKnownClass(
				"javax.net.ssl.SSLSocketFactory", mthsSSLFactory);

		String[] mthsSFactory = { "createSocket" }; // "close"
		honeyPot.registerKnownClass("javax.net.SocketFactory",
				mthsSFactory);

		// ---------------------------------ACTIVITY------------------------------------

		String[] mthsActivity = { "onCreate", "onStart", "onRestart",
				"onResume", "onPause", "onStop", "onDestroy" };
		honeyPot.registerKnownClass("android.app.Activity",
				mthsActivity);

		// ---------------------------------GPS-----------------------------------------

		// missing stuff :
		// http://developer.android.com/reference/android/location/LocationManager.html
		// http://developer.android.com/reference/android/location/package-summary.html
		String[] mthsLocationManager = { "init", "requestLocationUpdates",
				"requestSingleUpdate", "getProviders", "getProvider",
				"getLastKnownLocation", "getGpsStatus" };
		honeyPot.registerKnownClass(
				"android.location.LocationManager", mthsLocationManager);

		String[] mthsLocation = { "init", "getLongitude", "getLatitude",
				"getAltitude" };
		honeyPot.registerKnownClass("android.location.Location",
				mthsLocation);

		// ----------------------------ACCELEROMETER-------------------------------------

		String[] mthsSensor = { "init", "registerListener" };
		honeyPot.registerKnownClass(
				"android.hardware.SensorManager", mthsSensor);

		// ----------------------------CAMERA--------------------------------------------

	}
	
	/*
	 * Matches a class signature with the known packages
	 */
	public static ArrayList<String> getTagsPerClass(String cls) {
		ArrayList<String> ret = null;

		Set<String> pknames = _tagsPerPackage.keySet();

		for (String pkn : pknames) {
			if (cls.contains(pkn))
				return _tagsPerPackage.get(pkn);
		}

		return ret;
	}
	
	public static ArrayList<String> getTagsPerClass(SmaliNameEncoding nameSchema, Integer cls) {
		
		return getTagsPerClass(nameSchema.getName(cls));
	}
	
	public static boolean shouldIgnoreMethod(String cls, String method){
		String clsmth = cls + method;
		for(String mth : _ignores){
			if(clsmth.contains(mth))
				return true;
		}
		return false;
	}
	
	public static boolean shouldIgnoreMethod(SmaliNameEncoding nameSchema, Integer cls, Integer method){
		return shouldIgnoreMethod(nameSchema.getName(cls), nameSchema.getName(method));
	}

	/*
	 * Returns the packages associated with a tag
	 */
	public static ArrayList<String> getPackagesPerTag(String tag) {
		return _packagesPerTag.get(tag);
	}

	/*
	 * Loads info from a file that contains all android packages and has tags
	 * such as: -hardware, -localfiles, -ui, -gps, -internet, -infosource, -os,
	 * -reflection, -threading
	 * 
	 * ONLY STORES PACKAGES WITH DEFINED TAGS!
	 */
	public static void loadKnownPackageTags() {
		File file = new File(_packagesPath);

		Scanner scanner;
		try {
			scanner = new Scanner(file);
			ArrayList<String> tags = new ArrayList<String>();
			String pckname = "";
			int tagCounter = 0;

			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				line = line.trim();

				if (line.length() <= 0)
					continue;

				Character cmd = line.charAt(0);

				switch (cmd) {
				case '>':
					tagCounter = 0; // only stores packages with tags
					pckname = line.substring(1);
					tags = new ArrayList<String>();
					break;
				case '-':
					String tg = line.substring(1);
					tags.add(tg);

					// store in respective package
					if (tagCounter == 0) {
						_tagsPerPackage.put(pckname, tags);
					}

					// store in respective tag
					if (_packagesPerTag.containsKey(tg)) {
						_packagesPerTag.get(tg).add(pckname);
					} else {
						ArrayList<String> aux = new ArrayList<String>();
						aux.add(pckname);
						_packagesPerTag.put(tg, aux);
					}
					tagCounter++;
					break;
				case '#':
					break;
				default:
					System.out
							.println("[WARNING] loadPackageKnowledgeFile : found unknown line - "
									+ line);
					break;
				}

				
			}

			scanner.close();
			System.out
					.println("Loaded known package tags:------------------------");
			for (String s : _tagsPerPackage.keySet()) {
				SmaliLogger.log("loadKnownPackageTags","Loaded package : " + s);
				for (String tg : _tagsPerPackage.get(s)) {
					SmaliLogger.log("loadKnownPackageTags","--->Tag : " + tg);
				}
			}

			System.out
					.println("--------------------------------------------------");

			for (String tg : _packagesPerTag.keySet()) {
				SmaliLogger.log("loadKnownPackageTags","Loaded Tag : " + tg);
				for (String pck : _packagesPerTag.get(tg)) {
					SmaliLogger.log("loadKnownPackageTags","--->Package : " + pck);
				}
			}
			System.out
					.println("--------------------------------------------------");

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void loadIgnored() {
		File file = new File(_ignoresPath);

		Scanner scanner;

		try {
			scanner = new Scanner(file);

			while (scanner.hasNextLine()) {
				String line = scanner.nextLine().trim();
				
				if (line.length() <= 0)
					continue;

				Character cmd = line.charAt(0);

				switch (cmd) {
				case '>':
					_ignores.add(line.substring(1));
					break;

				default:
					break;
				}
				
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out
		.println("Ignored matches:----------------------------------");
		for(String ignore : _ignores){
			SmaliLogger.log("loadIgnored","Ignoring : " + ignore);
		}
		System.out
		.println("--------------------------------------------------");

	}

}
