package com.aknahs.honeypots;

import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

import com.aknahs.smali.representations.SmaliClassRepresentation;
import com.aknahs.smali.representations.SmaliMethodRepresentation;
import com.aknahs.smali.representations.SmaliNameEncoding;

public class SmaliHoneyPots {

	private HashMap<Integer, SmaliClassRepresentation> _knownClasses = new HashMap<Integer, SmaliClassRepresentation>();
	
	private SmaliNameEncoding _nameSchema;
	
	public SmaliHoneyPots(SmaliNameEncoding nameSchema){
		_nameSchema = nameSchema;
	}
	
	public  Collection<SmaliClassRepresentation> getSmaliKnownClasses() {
		return _knownClasses.values();
	}

	public  SmaliClassRepresentation getSmaliClass(String classname) {
		Integer id = _nameSchema.getID(classname);
		return _knownClasses.get(id);
	}
	
	public  SmaliClassRepresentation getSmaliClass(Integer id) {
		return _knownClasses.get(id);
	}

	public  Set<Integer> getSmaliClassNames() {
		return _knownClasses.keySet();
	}

	private  SmaliClassRepresentation createKnownClass(String className,
			String[] mths) {

		SmaliClassRepresentation cls = new SmaliClassRepresentation(_nameSchema,className);

		for (String s : mths)
			cls.registerMethod(new SmaliMethodRepresentation(_nameSchema,className, s));

		return cls;
	}

	public  void registerKnownClass(String className, String[] mths) {
		SmaliClassRepresentation cls = createKnownClass(className, mths);
		_knownClasses.put(cls.getClassID(), cls);
	}
	
	public  SmaliMethodRepresentation registerKnownMethod(Integer className, Integer mth) {
		return registerKnownMethod(_nameSchema.getName(className), _nameSchema.getName(mth));
	}

	public  SmaliMethodRepresentation registerKnownMethod(String className, String mth) {
		SmaliClassRepresentation cls = getSmaliClass(className);

		if (cls == null) { //register both class and method
			String[] mths = { mth };
			cls = createKnownClass(className, mths);
			_knownClasses.put(cls.getClassID(), cls);
			return cls.getMethod(mth);
		}
		else{//register just the method
			SmaliMethodRepresentation honeyMth = cls.getMethod(mth);
			
			if(honeyMth!=null)
				return honeyMth; //Method was already registered!
			
			SmaliMethodRepresentation newmth = new SmaliMethodRepresentation(_nameSchema,cls.getClassName(), mth);
			cls.registerMethod(newmth);
			return newmth;
		}
	}
	
	public  void clearHoneyPotLinks() {
		for (SmaliClassRepresentation cls : _knownClasses.values()) {
			cls._traversed = false;
			cls.clearLinksToSuperMethods();
			for (SmaliMethodRepresentation mth : cls.getMethods())
				mth.clearLinks();
		}
	}

}
