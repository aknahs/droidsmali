package com.aknahs.injection;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.Scanner;

import com.aknahs.smali.opcodes.SmaliDirectives;
import com.aknahs.smali.representations.SmaliApkRepresentation;
import com.aknahs.smali.representations.SmaliClassRepresentation;
import com.aknahs.smali.representations.SmaliMethodRepresentation;

public class CodeInjection {

	public static String DEFAULT_APKTOOL = "apktool";
	public static String DEFAULT_KEYSTORE = "/home/aknahs/.android/debug.keystore";

	public static String _apktoolPath = null;
	public static String _apkFile = null;
	public static String _jsonFile = null;
	public static String _keyStoreFile = null;

	public static String firstFilePath = null;
	public static String firstFileName = null;

	public static String _insertClass = null;

	// startMethodTracing("mytrace", 100000000);
	public static String startMethodTracingCode(String traceName) {
		String res = "\tconst-string v1, \"" + traceName + "\"\n\n";
		res += "\tinvoke-static {v1}, Landroid/os/Debug;->startMethodTracing(Ljava/lang/String;)V\n";
		return res;
	}

	public static String stopMethodTracingCode() {
		return "\tinvoke-static {}, Landroid/os/Debug;->stopMethodTracing()V\n";
	}

	public static String createOnDestroyCode(String superClass) {
		String res = ".method protected onDestroy()V\n";
		res += "\t.locals 0\n\n\t.prologue\n";
		res += "\tinvoke-super {p0}, L" + superClass + ";->onDestroy()V\n\n";
		res += stopMethodTracingCode();
		res += "\n\treturn-void\n.end method";

		return res;
	}

	// requires at least 5 locals!!
	public static String logLine(String mainActivity, String cls, String mth,
			String args, String ret) {
		return "\tconst/4 v0, 0x0\n\n"
				+ "\tconst-string v1, \""
				+ cls
				+ "\"\n\n"
				+ "\tconst-string v2, \""
				+ mth
				+ "\"\n\n"
				+ "\tconst-string v3, \""
				+ args
				+ "\"\n\n"
				+ "\tconst-string v4, \""
				+ ret
				+ "\"\n\n"
				+ "\tinvoke-static {v0, v1, v2, v3, v4}, L"
				+ mainActivity
				+ ";->logEvent(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V\n\n";
	}

	public static String createLogCode() {
		String code = "";
		Charset charset = Charset.forName("UTF-8");
		try (BufferedReader reader = Files.newBufferedReader((new File(
				"loggercode")).toPath(), charset)) {
			String line = null;
			while ((line = reader.readLine()) != null) {
				code += line + "\n";
			}
		} catch (IOException x) {
			System.err.format("IOException: %s%n", x);
		}
		System.out.println("Injecting code: \n " + code);
		return code;
	}

	/*
	 * private static boolean processMainArgs(String[] args) { int len =
	 * args.length; int processedArgs = 0;
	 * 
	 * if (len <= 0) { System.out .println(
	 * "Usage : java -jar trapmaster-agent.java -a apktool_path -k keystore"); }
	 * 
	 * while (processedArgs < len) { switch (args[processedArgs]) { case "-a":
	 * if (len < processedArgs + 2) { System.out
	 * .println("[ERROR] -a expects a apktool_path argument"); return false; }
	 * _apktoolPath = args[processedArgs + 1];
	 * System.out.println("Setting apktool_path to " + args[processedArgs + 1]);
	 * processedArgs += 2; break; case "-k": if (len < processedArgs + 2) {
	 * System.out .println("[ERROR] -k expects a keyStore_path argument");
	 * return false; } _keyStoreFile = args[processedArgs + 1];
	 * System.out.println("Setting keystore to " + args[processedArgs + 1]);
	 * processedArgs += 2; break; case "-j": if (len < processedArgs + 2) {
	 * System.out .println("[ERROR] -j expects a json_path argument"); return
	 * false; } processedArgs += 2; break;
	 * 
	 * default: if (args[processedArgs].contains(".apk")) { _apkFile =
	 * args[processedArgs]; System.out.println("Loading apk : " + _apkFile);
	 * processedArgs++; } else { System.out .println(
	 * "Wrong Arguments! Usage : java -jar smaliParser.java -a apktool_path apk_name.json"
	 * ); return false; } } }
	 * 
	 * return true; }
	 */

	public static boolean injectLogCodeOnAllMethods(String outfile,
			String infile, Integer neededLocals) {

		File activity = new File(infile);
		Scanner scanner = null;

		// System.out.println("Injecting code on all methods");

		boolean isInjected = false;
		boolean insideMethod = false;
		boolean waitUntilValueConcluded = false;

		String cls = "";
		String mth = "";
		String args = "";
		String ret = "";

		try {
			scanner = new Scanner(activity);
			PrintWriter writer = new PrintWriter(outfile, "UTF-8");
			String line = "";

			/*
			 * // After this loop, line has the first header of the method for
			 * (short counter = 0; counter <= mth.getLine(); counter++) { line =
			 * scanner.nextLine(); writer.println(line); }
			 */

			// System.out.println("#0-" + line);

			while (scanner.hasNextLine()) {
				line = scanner.nextLine();
				String trimmedLine = line.trim();

				if (SmaliDirectives.isDirective(line)) {

					String[] splits = trimmedLine.split(" ");

					switch (SmaliDirectives.convertDirective(splits[0])) {

					case LOCALS:
						Integer locals = Integer.valueOf(splits[1]);
						writer.println("\t.locals "
								+ ((locals < neededLocals) ? neededLocals
										: locals));
						break;
					case CLASS:
						writer.println(line);
						for (int i = 0; i < splits.length; i++) {
							String c = splits[i].trim();
							if (c.charAt(0) == 'L') {
								cls = c.substring(1).replace(";", "");
								break;
							}
						}
						System.out.println("Processing class : " + cls);
						if (_insertClass == null)
							_insertClass = cls;
						break;
					case METHOD:
						writer.println(line);
						insideMethod = true;
						isInjected = false;
						String c = "";
						Integer idx = 0;

						System.out.println("Processing method in line : "
								+ line);

						for (int i = 0; i < splits.length; i++) {
							c = splits[i].trim();
							idx = c.indexOf("(");
							if (idx >= 0) {
								mth = c.substring(0, idx);
								break;
							}
						}

						System.out.println("Method name : " + mth);

						Integer idx2 = c.indexOf(")");
						args = c.substring(idx + 1, idx2);
						ret = c.substring(idx2 + 1);

						System.out.println("Args : " + args + " return : "
								+ ret);

						break;
						
					case ANNOTATION:
						writer.println(line);
						waitUntilValueConcluded = true;
						break;
					case END:
						if (waitUntilValueConcluded
								&& splits[1].contains("annotation"))
							waitUntilValueConcluded = false;
						else {
							if (splits[1].contains("method"))
								insideMethod = false;
						}

						writer.println(line);
						break;
					default:
						writer.println(line);
						break;
					}
				} else {

					if (waitUntilValueConcluded) {

					} else {

						if (insideMethod && !isInjected
								&& trimmedLine.length() > 0
								&& !(trimmedLine.charAt(0) == '#')
								&& !trimmedLine.contains("invoke-super")) {

							writer.println(logLine(_insertClass, cls, mth,
									args, ret));
							isInjected = true;
						}
					}

					writer.println(line);
				}
			}

			scanner.close();
			writer.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if (scanner != null)
				scanner.close();
			return false;
		}

		return true;
	}

	public static boolean appendCodeOnClass(String newFileName,
			String activityFileName, String code) {
		System.out.println("Will append on " + activityFileName);
		File activity = new File(activityFileName);
		Scanner scanner = null;

		System.out.println("Appending code to " + activityFileName);
		try {
			scanner = new Scanner(activity);
			PrintWriter writer = new PrintWriter(newFileName, "UTF-8");
			String line = "";

			while (scanner.hasNext()) {
				line = scanner.nextLine();
				writer.println(line);
			}

			System.out.println("#0-" + line);

			writer.println();
			writer.println(code);

			scanner.close();
			writer.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if (scanner != null)
				scanner.close();
			return false;
		}

		return true;
	}

	public static boolean injectLogEverywhere(String smaliRootPath) {
		File root = new File(smaliRootPath);
		File[] list = root.listFiles();

		if (list == null) {
			System.out.println("Could not read smali root path");
			return false;
		}

		for (File f : list) {
			if (f.isDirectory()) {
				injectLogEverywhere(f.getAbsolutePath());

			} else {
				if (firstFilePath == null) {
					firstFilePath = new String(f.getAbsolutePath());
					firstFileName = new String(f.getName());
					System.out.println("###Setting paths : " + firstFilePath
							+ " : " + firstFileName);
				}
				injectLogCodeOnAllMethods("temp/" + f.getName() + "-altered",
						f.getAbsolutePath(), 5);
				replaceOriginalSmali(f.getAbsolutePath(), "temp/" + f.getName()
						+ "-altered");
			}
		}

		return true;
	}

	public static boolean injectCodeOnMethod(String newFileName,
			String activityFileName, SmaliMethodRepresentation mth, String code) {

		File activity = new File(activityFileName);
		Scanner scanner = null;

		System.out.println("Injecting code on method : " + mth.getMethodName());

		boolean isInjected = false;

		try {
			scanner = new Scanner(activity);
			PrintWriter writer = new PrintWriter(newFileName, "UTF-8");

			if (mth.getLocals() <= 0) {
				// TODO: inject locals :)
				System.out
						.println("Could not insert on method because there were no locals : "
								+ mth.getLocals());
				scanner.close();
				writer.close();
				return false;
			}

			String line = "";

			// After this loop, line has the first header of the method
			for (short counter = 0; counter <= mth.getLine(); counter++) {
				line = scanner.nextLine();
				writer.println(line);
			}

			System.out.println("#0-" + line);

			while (scanner.hasNextLine()) {
				line = scanner.nextLine();
				if (SmaliDirectives.isDirective(line)) {
					String[] splits = line.trim().split(" ");

					switch (SmaliDirectives.convertDirective(splits[0])) {

					case CLASS:
						writer.println(line);
						break;
					case METHOD:
						writer.println(line);
						break;
					default:
						writer.println(line);
						break;
					}
				} else {
					if (!isInjected && line.trim().length() > 0) {
						writer.println(code);
						isInjected = true;
					}

					writer.println(line);
				}
			}

			scanner.close();
			writer.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if (scanner != null)
				scanner.close();
			return false;
		}

		return true;
	}

	public static boolean injectDebugCode(SmaliApkRepresentation apk) {
		System.out.println("Injecting Debug Code");
		String activityName = apk.getManifest()._mainActivity;

		if (activityName == null || activityName.length() <= 0) {
			System.out
					.println("[ERROR] : injectDebugCode - activity was empty");
			return false;
		}

		if (activityName.charAt(0) == '.') {
			activityName = apk.getManifest()._apkname.replace('.', '/')
					+ activityName.replace('.', '/');
			System.out.println("Preceding activity name with package name : "
					+ activityName);
		}

		// System.out.println(apk.getNameEncoding());
		// System.out.println(apk);

		System.out.println("Reading class from json - "
				+ activityName.replace('/', '.'));

		SmaliClassRepresentation mainCls = apk.getClass(activityName.replace(
				'/', '.'));

		if (mainCls == null) {
			System.out
					.println("[ERROR] : injectDebugCode - Couldnt retrieve main class from json");
			return false;
		}

		String activityFileName = "results/" + _apkFile + "/smali/"
				+ activityName.replace(".", "/") + ".smali";
		System.out.println("Opening activity : " + activityFileName);

		SmaliMethodRepresentation onCreateMethod = mainCls
				.getMethod("onCreate");
		SmaliMethodRepresentation onDestroyMethod = mainCls
				.getMethod("onDestroy");

		if (onCreateMethod != null) {
			// The onCreate method exists and we will inject code on it
			System.out.println("Found onCreate");
			System.out.println("--> Locals : " + onCreateMethod.getLocals());
			System.out.println("--> Line   : " + onCreateMethod.getLine());
			if (!injectCodeOnMethod("results/temp.smali", activityFileName,
					onCreateMethod, startMethodTracingCode(apk.getApkName())))
				return false;
		} else {
			// There was no onCreate method
			// (wonder if this ever happens...
			// it could in theory but then the activity probably would do
			// nothing:))

			System.out
					.println("[ERROR] : injectDebugCode - Main Activity has no onCreate");
		}

		if (onDestroyMethod != null) {
			System.out.println("onDestroy found");
			System.out.println("--> Locals : " + onDestroyMethod.getLocals());
			System.out.println("--> Line   : " + onDestroyMethod.getLine());
			if (!injectCodeOnMethod("results/new.smali", "results/temp.smali",
					onDestroyMethod, stopMethodTracingCode()))
				return false;
		} else {
			if (!appendCodeOnClass(
					"results/new.smali",
					"results/temp.smali",
					createOnDestroyCode(mainCls.getSuperClassName().replace(
							".", "/"))))
				return false;
		}

		return replaceOriginalSmali(activityFileName, "results/new.smali");
	}

	public static boolean replaceOriginalSmali(String copyTo, String copyFrom) {
		File copyToFile = new File(copyTo);
		File copyFromFile = new File(copyFrom);

		try {
			InputStream inStream = new FileInputStream(copyFromFile);
			OutputStream outStream = new FileOutputStream(copyToFile);

			byte[] buffer = new byte[1024];

			int length;
			// copy the file content in bytes
			while ((length = inStream.read(buffer)) > 0)
				outStream.write(buffer, 0, length);

			inStream.close();
			outStream.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return true;
	}

	public static boolean runCommand(String[] args) {
		Process runProcess;
		String line;

		try {

			runProcess = Runtime.getRuntime().exec(args);
			System.out.println("WaitFor");
			// runProcess.waitFor();

			System.out.println("Reading InputStream");

			BufferedReader in1 = new BufferedReader(new InputStreamReader(
					runProcess.getInputStream()));

			while ((line = in1.readLine()) != null) {
				System.out.println(line);
			}

			in1.close();

			System.out.println("Reading ErrorStream");
			BufferedReader in = new BufferedReader(new InputStreamReader(
					runProcess.getErrorStream()));

			while ((line = in.readLine()) != null) {
				System.out.println(line);
			}

			in.close();
			System.out.println("Done signing");

		} catch (IOException e) {
			return false;
			/*
			 * } catch (InterruptedException e) { // TODO Auto-generated catch
			 * block return false;
			 */
		}
		return true;
	}

	public static boolean compileAndSignAPK(String apkName,
			SmaliApkRepresentation apk) {
		File file = new File("results/" + apkName);
		System.out.println("Compiling : " + file.getPath());

		boolean ret = runCommand(new String[] { _apktoolPath, "b", "-f",
				file.getPath(), "results/" + apk.getApkName() + "-debug.apk" });

		if (ret != true)
			return false;

		System.out.println("Compiled into : " + "results/" + apk.getApkName()
				+ "-debug.apk");

		System.out.println("Signing application");

		ret = runCommand(new String[] { "jarsigner", "-verbose", "-sigalg",
				"MD5withRSA", "-digestalg", "SHA1", "-keystore", _keyStoreFile,
				"-storepass", "android", "-signedjar",
				"results/" + apk.getApkName() + "-debug-signed.apk",
				"results/" + apk.getApkName() + "-debug.apk", "androiddebugkey" });

		System.out.println("Signed application : " + "results/"
				+ apk.getApkName() + "-debug-signed.apk");

		return ret;
	}

	public static boolean compileAndSignAPK(String apkName) {
		File file = new File("results/" + apkName);
		System.out.println("Compiling : " + file.getPath());

		boolean ret = runCommand(new String[] { _apktoolPath, "b", "-f",
				file.getPath(), "results/" + apkName + "-debug.apk" });

		if (ret != true)
			return false;

		System.out.println("Compiled into : " + "results/" + apkName
				+ "-debug.apk");

		System.out.println("Signing application");

		ret = runCommand(new String[] { "jarsigner", "-verbose", "-sigalg",
				"MD5withRSA", "-digestalg", "SHA1", "-keystore", _keyStoreFile,
				"-storepass", "android", "-signedjar",
				"results/" + apkName + "-debug-signed.apk",
				"results/" + apkName + "-debug.apk", "androiddebugkey" });

		System.out.println("Signed application : " + "results/" + apkName
				+ "-debug-signed.apk");

		return ret;
	}

	public static boolean decompileAPK(String apk) {
		File file = new File("apks/" + apk);

		System.out.println("Decompiling : " + file.getPath());

		return runCommand(new String[] { _apktoolPath, "d", "-f", "-r",
				file.getPath(), "results/" + apk });
	}

	/*
	 * public static void main(String[] args) {
	 * 
	 * if (!processMainArgs(args)) {
	 * System.out.println("Wrong arguments. Terminating."); return; }
	 * 
	 * if(_keyStoreFile == null){ System.out.println(
	 * "[WARNING] Assuming keystore path. Should be specified with -k flag");
	 * _keyStoreFile = DEFAULT_KEYSTORE; }
	 * 
	 * if(_apktoolPath == null){ System.out.println(
	 * "[WARNING] Assuming apktool path. Should be specified with -a flag");
	 * _apktoolPath = DEFAULT_APKTOOL; }
	 * 
	 * if (_apkFile == null) { File dir = new File("apks/");
	 * 
	 * File[] files = dir.listFiles();
	 * 
	 * if(files == null){ System.out.println("The apks/ folder was empty.");
	 * return; }
	 * 
	 * int index = 0; ArrayList<String> apks = new ArrayList<String>(); for
	 * (File f : files) { if (f.getName().contains(".apk")) {
	 * System.out.println(index + " - " + f.getName()); apks.add(f.getName());
	 * index++; } }
	 * 
	 * if (index == 0) {
	 * System.out.println("There are no apk files in apks/ folder"); return; }
	 * 
	 * System.out.print("Choose an option [0" + (index <= 1 ? "" : ("..." +
	 * (index - 1))) + "]: "); Scanner in = new Scanner(System.in); int num =
	 * in.nextInt(); in.close();
	 * 
	 * if (num < 0 || num > index - 1) { System.out.println("Wrong option.");
	 * return; }
	 * 
	 * System.out.println("Loading option " + num + " - " + apks.get(num));
	 * _apkFile = apks.get(num); }
	 * 
	 * if (_jsonFile == null) {
	 * 
	 * _jsonFile = _apkFile.replace(".apk", "").concat(".json"); System.out
	 * .println("No json file specified. Assuming same name as apk: " +
	 * _jsonFile); }
	 * 
	 * SmaliApkRepresentation apk = loadJSON(_jsonFile);
	 * 
	 * if (!decompileAPK(_apkFile)) {
	 * System.out.println("Failed decompiling application"); return; }
	 * 
	 * injectDebugCode(apk);
	 * 
	 * if(!compileAndSignAPK(_apkFile, apk)){
	 * System.out.println("Failed compiling application"); return; }
	 * 
	 * System.out.println("Done."); }
	 */

}
