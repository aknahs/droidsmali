package com.aknahs.log;

import java.lang.reflect.Method;

public class SmaliLogger {
	private static LoggerAbstractImplementation _logger = null;

	private static void attemptSettingLog() {
		if (_logger == null) {
			try { /*TODO: this should be sync*/
				// need to find a new logger. Let's check if we have Android
				// running
				System.out.println(System.getProperty("java.runtime.name"));
				if (System.getProperty("java.runtime.name").toLowerCase()
						.contains("android")) {
					Class<?> c = Class.forName("android.util.Log");
					Method m = c.getMethod("v", String.class, String.class);
					_logger = new AndroidLoggerImplementation(m);
					_logger.log("SmaliLogger",
							"Found! Instantiating Android-based logger");
				} else {
					// fallback option, system logger.
					_logger = new JavaLoggerImplementation();
					_logger.log("SmaliLogger",
							"Instantiating System-based logger");
				}
			} catch (Exception e) {
				_logger = new JavaLoggerImplementation();
				_logger.log("SmaliLogger",
						"Failed to retrieve vm name. Using java logger. Exception : "
								+ e.getMessage());
			}
			_logger.log("SmaliLogger", "Configuration done!");
		}
	}

	public static void log(String location, String message) {
		attemptSettingLog();
		if (_logger._isActive)
			_logger.log(location, message);
	}

	public static void setLogger(LoggerAbstractImplementation logger) {
		_logger = logger;
	}

	public static void error(String location, String message) {
		attemptSettingLog();
		_logger.log("ERROR@" + location, message);
	}

	public static void setTag(String tag) {
		attemptSettingLog();
		_logger.setTag(tag);
	}
	
	public static void disableLog(){
		attemptSettingLog();
		_logger.disableLogs();
	}

}
