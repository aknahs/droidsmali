package com.aknahs.log;

public class JavaLoggerImplementation extends LoggerAbstractImplementation {

	public JavaLoggerImplementation(String tag) {
		setTag(tag);
	}

	public JavaLoggerImplementation() {
	}

	@Override
	public void log(String location, String message) {
		System.out.println(location + " : " + message);
	}

}
