package com.aknahs.log;

public abstract class LoggerAbstractImplementation {

	protected String _tag = "TRAP-DEFAULT";
	protected boolean _isActive = true;
	
	public void setTag(String tag){
		_tag = tag;
	}
	
	public String getTag(){
		return _tag;
	}
	
	public void disableLogs(){
		_isActive = false;
	}
	
	public abstract void log(String location, String message);

}
