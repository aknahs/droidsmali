package com.aknahs.log;

import java.lang.reflect.Method;


public class AndroidLoggerImplementation extends LoggerAbstractImplementation{

	private Method _method = null;
	
	public AndroidLoggerImplementation(String tag, Method mth){
		setTag(tag);
		_method = mth;
	}
	
	public AndroidLoggerImplementation(Method mth){
		_method = mth;
	}
	
	protected AndroidLoggerImplementation(){
	}
	
	@Override
	public void log(String location, String message) {
		try {
			_method.invoke(null,_tag, location + " : " + message);
        } catch(Exception e) {
            System.out.println(message);
            System.out.println("Problem printing: " + e);
        }
		
	}

}
