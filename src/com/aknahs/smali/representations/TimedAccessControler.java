package com.aknahs.smali.representations;

import java.util.Date;

import com.google.gson.annotations.Expose;

public class TimedAccessControler extends AccessController{

	//deltas of method times should fit well in longs (2^63 > 10^18).
	//if the method runs for a lonnnng period, e.g. 1 year, it takes 10^7 digits.
	//if it runs very often it runs for a short period. 
	//So imagine 3 seconds times 1 billion executions..3*10^12 still fits.
	//As the multiplier is actually the time.. we can say time is on our side.
	
	/*public BigInteger registerTime(Date init, Date end){
		deltaTotalTime += (end.getTime() - end.);
		return deltaTotalTime;
	}*/
	
	@Expose private long deltaTotalTime = 0;
	@Expose private long numberDeltas = 0;
	
	public long registerTime(Date init, Date end){
		deltaTotalTime += (end.getTime() - init.getTime());
		numberDeltas++;
		return deltaTotalTime;
	}
	
	public void resetAverage(){
		deltaTotalTime = 0;
		numberDeltas = 0;
	}
	
	public long getAverageTime(){
		if(deltaTotalTime == 0)
			return 0;
		return (deltaTotalTime/numberDeltas);
	}
}
