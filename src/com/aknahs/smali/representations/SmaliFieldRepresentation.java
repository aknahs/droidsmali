package com.aknahs.smali.representations;

import java.lang.reflect.Modifier;

import com.google.gson.annotations.Expose;

public class SmaliFieldRepresentation extends SmaliRepresentation{

	@Expose
	private Integer _fieldClassID;
	@Expose
	private Integer _fieldID;
	@Expose
	private Integer _modifier;
	
	private SmaliFieldRepresentation(RepresentationType type) {
		super(type);
	}
	
	public SmaliFieldRepresentation(SmaliNameEncoding encoding, Integer classID, Integer fieldID, Integer modifier) {
		super(RepresentationType.FIELD);
		_fieldClassID = classID;
		_fieldID = fieldID;
		_modifier = modifier;
		_nameSchema = encoding;
	}
	
	public SmaliFieldRepresentation(SmaliNameEncoding encoding, Integer classID, String fieldName, Integer modifier) {
		super(RepresentationType.FIELD);
		_fieldClassID = classID;
		_fieldID = encoding.registerID(fieldName);
		_modifier = modifier;
		_nameSchema = encoding;
	}

	public Integer getID(){
		return _fieldID;
	}
	
	public Integer getClassID(){
		return _fieldClassID;
	}
	
	public String getFieldName(){
		return _nameSchema.getName(_fieldClassID);
	}
	
	public Integer getModifier(){
		return _modifier;
	}
	
	public String toString(){
		return "Field:ClassID-" + _fieldClassID + "|Field-" + _nameSchema.getName(_fieldID) + "|modifier" + _modifier; 
	}
	
}
