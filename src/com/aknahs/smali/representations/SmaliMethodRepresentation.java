package com.aknahs.smali.representations;

import java.util.ArrayList;
import java.util.Collection;

import com.google.gson.annotations.Expose;

public class SmaliMethodRepresentation extends SmaliRepresentation {
	@Expose
	private Integer _methodID;
	@Expose
	private Integer _ownerClassID; // cannot store reference because of gson

	@Expose
	private short _line = -1;
	@Expose
	private short _locals = -1;

	private ArrayList<SmaliLinkRepresentation> _linksToThisMethod = new ArrayList<SmaliLinkRepresentation>();

	@Expose
	private ArrayList<SmaliLinkRepresentation> _linksToOtherMethods = new ArrayList<SmaliLinkRepresentation>();

	@Expose public TimedAccessControler _accesses = new TimedAccessControler(); 
	
	public short getLine(){
		return _line;
	}
	
	public short getLocals(){
		return _locals;
	}
	
	public SmaliMethodRepresentation(SmaliNameEncoding schema, String owner,
			String methodname, short line, short locals) {
		super(RepresentationType.METHOD);
		setNameEncoding(schema);
		_methodID = getNameEncoding().registerID(methodname);
		_ownerClassID = getNameEncoding().registerID(owner);
		_line = line;
		_locals = locals;
	}

	public SmaliMethodRepresentation(SmaliNameEncoding schema, String owner,
			String methodname) {
		super(RepresentationType.METHOD);
		setNameEncoding(schema);
		_methodID = getNameEncoding().registerID(methodname);
		_ownerClassID = getNameEncoding().registerID(owner);
	}

	/*
	 * public SmaliMethodRepresentation(Integer owner, Integer methodname) {
	 * super(RepresentationType.METHOD); _methodID = methodname; _ownerClassID =
	 * owner; }
	 */

	public void setLine(short line) {
		_line = line;
	}

	public void setLocals(short locals) {
		_locals = locals;
	}

	public void clearLinks() {
		_linksToOtherMethods.clear();
		_linksToThisMethod.clear();
	}

	public Integer getOwnerClass() {
		return _ownerClassID;
	}

	public Integer getMethodID() {
		return _methodID;
	}

	public String getMethodName() {
		return getNameEncoding().getName(_methodID);
	}

	public Collection<SmaliLinkRepresentation> getLinks() {
		return _linksToOtherMethods;
	}

	public Collection<SmaliLinkRepresentation> getReverseLinks() {
		return _linksToThisMethod;
	}
	
	//TODO: Check if this messes with the registering os 
	public void createMultipleAccessesToAnotherMethod(Collection<SmaliLinkRepresentation> links){
		for(SmaliLinkRepresentation lnk : links)
			createAccessToAnotherMethod(lnk);
	}
	
	public void createMultipleAccessesToThisMethod(Collection<SmaliLinkRepresentation> links){
		for(SmaliLinkRepresentation lnk : links)
			createAccessToThisMethod(lnk);
	}

	public void createAccessToAnotherMethod(SmaliLinkRepresentation link) {

		int index = _linksToOtherMethods.indexOf(link);

		if (index < 0) {
			_linksToOtherMethods.add(link);
		} else {
			// TODO: we should avoid this being called during dynamic by mistake
			// register occurrence of t
			_linksToOtherMethods.get(index).registerStaticOccurence();
			// method occurrence occurs only on the invoked method
		}
	}

	public void createAccessToThisMethod(SmaliLinkRepresentation link) {

		int index = _linksToThisMethod.indexOf(link);

		if (index < 0)
			_linksToThisMethod.add(link);
		else { // TODO: we should avoid this being called during dynamic by
				// mistake register occurrence of the link
			_linksToThisMethod.get(index).registerStaticOccurence();
			// register occurrence of this method
		}
	}

	public void registerDynamicAccessToAnotherMethod(String cls, String mth) {
		registerDynamicAccess(_linksToOtherMethods, cls, mth);
	}

	public void registerDynamicAccessToThisMethod(String cls, String mth) {
		registerDynamicAccess(_linksToThisMethod, cls, mth);
	}

	public void registerDynamicAccess(ArrayList<SmaliLinkRepresentation> lst,
			String cls, String mth) {

		SmaliLinkRepresentation lnk = new SmaliLinkRepresentation(
				getNameEncoding(), cls, mth);
		int index = lst.indexOf(lnk);

		if (index >= 0) {
			lst.get(index).registerStaticOccurence();
			this.registerStaticOccurence();
		} else {
			// TODO: this should not happen
			System.out
					.println("[ERROR] SmaliMethodRepresentation.registerAccess : method was not predicted! -> "
							+ cls + "." + mth);
			lst.add(lnk);
		}
	}
}
