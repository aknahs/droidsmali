package com.aknahs.smali.representations;

import com.google.gson.annotations.Expose;

public class SmaliRepresentation {
	public static enum RepresentationType {
		CLASS, METHOD, LINK, FIELD
	}
	
	@Expose private RepresentationType _type;
	
	@Expose private int _staticOccurencies = 1;
	
	//TODO: deprecated since accessController has the number of deltas
	private int _dynamicOccurencies = 0;
	
	public Boolean _traversed = false;
	public Boolean _tainted = false;
	public Boolean _hooked = false;
	
	protected SmaliNameEncoding _nameSchema;
	
	//@Expose public AccessController _accesses;
	
	public void revive(SmaliNameEncoding encoding){
		setNameEncoding(encoding);
		_traversed = false;
		_tainted = false;
		_hooked = false;
	}
	
	public void setNameEncoding(SmaliNameEncoding encoding){
		_nameSchema = encoding;
	}
	
	public SmaliNameEncoding getNameEncoding(){
		return _nameSchema;
	}
	
	public SmaliRepresentation(RepresentationType type){
		_type = type;
		
		/*
		if(type == RepresentationType.METHOD)
			_accesses = new TimedAccessControler();
		else
			_accesses = new AccessController();
		*/
	}
	
	public RepresentationType getType(){
		return _type;
	}
	
	public void registerStaticOccurence(){
		_staticOccurencies++;
	}
	
	public int getStaticOccurencies(){
		return _staticOccurencies;
	}
	
	public int getDynamicOccurencies(){
		return _dynamicOccurencies;
	}
	
	public void registerDynamicOccurence(){
		_dynamicOccurencies++;
	}
	
}
