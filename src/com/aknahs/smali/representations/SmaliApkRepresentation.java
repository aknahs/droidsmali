package com.aknahs.smali.representations;

import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

import com.aknahs.honeypots.SmaliHoneyPots;
import com.aknahs.manifest.ManifestRepresentation;
import com.google.gson.annotations.Expose;


/*SmaliRepresentations are not supposed to be a replacement for the capabilities of java reflection.
 * Instead they are an aid to understand the application behavior and enhance the possibilities of
 * hacking them.*/
public class SmaliApkRepresentation {
	@Expose private ManifestRepresentation _manifest;
	@Expose private HashMap<Integer, SmaliClassRepresentation> _classes;
	@Expose private SmaliNameEncoding _nameSchema;
	
	private SmaliHoneyPots _honeyPots;
	
	public ManifestRepresentation getManifest(){
		return _manifest;
	}
	
	public SmaliApkRepresentation(SmaliHoneyPots honeypots, SmaliNameEncoding nameSchema, ManifestRepresentation manifest) {
		_manifest = manifest;
		_classes = new HashMap<Integer, SmaliClassRepresentation>();
		_nameSchema = nameSchema;
		_honeyPots = honeypots;
	}
	
	public SmaliHoneyPots getHoneyPots(){
		return _honeyPots;
	}
	
	public SmaliNameEncoding getNameEncoding(){
		return _nameSchema;
	}

	public void registerClass(SmaliClassRepresentation cls) {
		_classes.put(cls.getClassID(), cls);
	}
	
	public HashMap<Integer, SmaliClassRepresentation> getHashClasses(){
		return _classes;
	}

	public Collection<SmaliClassRepresentation> getClasses() {
		return _classes.values();
	}

	public Set<Integer> getClassKeys() {
		return _classes.keySet();
	}

	public SmaliClassRepresentation getClass(String name) {
		Integer id = _nameSchema.getID(name);
		if(id == null)
			return null;
		return getClass(id);
	}
	
	public SmaliClassRepresentation getClass(Integer id) {
		if (_classes.containsKey(id))
			return _classes.get(id);
		else
			return null;
	}

	public String getApkName() {
		return _manifest._apkname;
	}
	
	/*
	 * The application json needs to re-establish some references and state. Some
	 * state just speeds execution (like the keyMap for name encoding) Other
	 * state are just references that make the apis easier.
	 */
	public void revive() {

		SmaliNameEncoding nameSchema = getNameEncoding();
		nameSchema.regenerateKeyMap(); // state
		_manifest._nameSchema = nameSchema;

		for (SmaliClassRepresentation cls : getClasses()) {
			cls.revive(nameSchema);// references
			for (SmaliMethodRepresentation mth : cls.getMethods()) {
				mth.revive(nameSchema);// references
				for (SmaliLinkRepresentation lnk : mth.getLinks()) {
					lnk.revive(nameSchema); // references
				}
			}
			for(SmaliFieldRepresentation field : cls.getFields()){
				field.revive(nameSchema);
			}
		}
	}

	@Override
	public String toString() {
		String ret = "APK = " + _manifest._apkname + "\n";
		for(SmaliClassRepresentation cls : _classes.values()){
			//ret += cls;
			ret += cls.getClassName()+"\t";
		}
		return ret;
	}
}
