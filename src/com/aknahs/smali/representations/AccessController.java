package com.aknahs.smali.representations;

import java.util.ArrayList;
import java.util.HashSet;

import com.google.gson.annotations.Expose;

public class AccessController {
	//e.g. UI GPS Sensors Sockets
	@Expose private HashSet<String> _tags = new HashSet<String>();

	public void registerTag(String tag){
		_tags.add(tag);
	}
	
	public void registerTags(ArrayList<String> tags){
		for(String tag : tags)
			registerTag(tag);
	}
	
	public boolean containsTag(String tag){
		return _tags.contains(tag);
	}
	
	public void registerTags(HashSet<String> tags){
		for(String tag : tags)
			registerTag(tag);
	}
	
	public void removeTag(String tag){
		_tags.remove(tag);
	}
	
	public HashSet<String> getTags(){
		return _tags;
	}
	
	public void removeTags(){
		_tags.clear();
	}
	
	public String getTagsString(){
		String ret = "";
		int counter = 0;
		for(String tag : _tags){
			if(counter>0)
				ret+=" | ";
			ret += tag;
			counter++;
		}
		return ret;
	}
	
	@Override
	public String toString(){
		String ret = "";
		for(String tag : _tags)
			ret += tag;
		return ret;
	}
}
