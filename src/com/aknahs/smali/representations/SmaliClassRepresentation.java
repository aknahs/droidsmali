package com.aknahs.smali.representations;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import com.aknahs.log.SmaliLogger;
import com.google.gson.annotations.Expose;

/*SmaliRepresentations are not supposed to be a replacement for the capabilities of java reflection.
 * Instead they are an aid to understand the application behavior and enhance the possibilities of
 * hacking them.*/

public class SmaliClassRepresentation extends SmaliRepresentation {

	/* Contains the class methods */
	@Expose
	private HashMap<Integer, SmaliMethodRepresentation> _methods = new HashMap<Integer, SmaliMethodRepresentation>();

	/* Contains the class fields */
	@Expose
	private HashMap<Integer, SmaliFieldRepresentation> _fields = new HashMap<Integer, SmaliFieldRepresentation>();

	// Some methods might be invoked in this class that are not implemented in
	// the _methods
	// They are implemented in the _superClass instead.
	@Expose
	private ArrayList<SmaliLinkRepresentation> _linksToThisClassSuperMethods = new ArrayList<SmaliLinkRepresentation>();

	/*
	 * Sometimes compiler breaks a class into multiple files (A$B), lets allow
	 * association between them
	 */
	@Expose
	private ArrayList<Integer> _innerClasses = new ArrayList<Integer>();

	@Expose
	private Integer _superClass = -1;

	@Expose
	private Integer _classname;

	@Expose
	public AccessController _accesses = new AccessController();

	public SmaliClassRepresentation(SmaliNameEncoding nameSchema,
			String classname) {
		super(SmaliRepresentation.RepresentationType.CLASS);
		_classname = nameSchema.registerID(classname);
		setNameEncoding(nameSchema);
	}

	public void registerMethod(SmaliMethodRepresentation mth) {
		Integer mthID = mth.getMethodID();
		if (!_methods.containsKey(mthID))
			_methods.put(mthID, mth);
		else {
			// TODO : perhaps register different parameters per method
			// Logger.log("Method was already registered : " +
			// _nameSchema.getName(mthID));
			_methods.get(mthID).createMultipleAccessesToAnotherMethod(mth.getLinks());
			_methods.get(mthID).createMultipleAccessesToThisMethod(mth.getReverseLinks());
		}
	}

	public void registerField(SmaliFieldRepresentation field) {
		Integer id = field.getID();

		if (!_fields.containsKey(id))
			_fields.put(id, field);
	}

	public void registerInnerClass(Integer id) {
		if (!_innerClasses.contains(id))
			_innerClasses.add(id);
	}

	public void clearLinksToSuperMethods() {
		_linksToThisClassSuperMethods.clear();
	}

	public Collection<SmaliMethodRepresentation> getMethods() {
		return _methods.values();
	}
	
	public Collection<SmaliFieldRepresentation> getFields() {
		return _fields.values();
	}
	
	public ArrayList<Integer> getInnerClasses() {
		return _innerClasses;
	}

	public String getClassName() {
		return getNameEncoding().getName(_classname);
	}

	public Integer getClassID() {
		return _classname;
	}

	public SmaliMethodRepresentation getMethod(String method) {
		Integer id = getNameEncoding().getID(method);
		if (id == null)
			return null;
		return _methods.get(id);
	}

	public SmaliMethodRepresentation getMethod(Integer id) {
		return _methods.get(id);
	}

	public String getSuperClassName() {
		return getNameEncoding().getName(_superClass);
	}

	public Integer getSuperClassID() {
		return _superClass;
	}

	public void setSuperClassName(String cls) {
		_superClass = getNameEncoding().registerID(cls);
	}

	public void createAccessToThisClassSuperMethods(SmaliLinkRepresentation link) {
		int index = _linksToThisClassSuperMethods.indexOf(link);
		// TODO: This contains is not needed once the different methods with
		// diff parameters are considered
		if (index < 0)
			_linksToThisClassSuperMethods.add(link);
		else {
			_linksToThisClassSuperMethods.get(index).registerStaticOccurence();
		}
	}

	@Override
	public String toString() {
		String ret = "CLASS = " + getClassName() + "\n";
		for (SmaliMethodRepresentation m : _methods.values()) {
			ret += "--> Method : " + m.getMethodName() + "\n";
		}
		return ret;
	}

}
