package com.aknahs.smali.representations;

import java.util.HashMap;

import com.google.gson.annotations.Expose;

public class SmaliNameEncoding {

	private HashMap<String, Integer> _keyMap = new HashMap<String, Integer>();
	@Expose private HashMap<Integer, String> _nameMap = new HashMap<Integer, String>();

	private Integer _nextID = 0;

	public void cleanMaps() {
		_keyMap.clear();
		_nameMap.clear();
	}
	
	public HashMap<Integer, String> getNameMap(){
		return _nameMap;
	}
	
	public void regenerateKeyMap(){
		if(_nameMap == null)
			return;
		
		for(Integer key : _nameMap.keySet()){
			String name = _nameMap.get(key);
			_keyMap.put(name, key);
		}
	}

	private Integer getKey(String name) {
		if (_keyMap.containsKey(name))
			return _keyMap.get(name);
		else {
			Integer id = _nextID;
			_keyMap.put(name, id);
			_nameMap.put(id, name);
			_nextID++;
			return id;
		}
	}

	public String getName(Integer key) {
		if (_nameMap.containsKey(key))
			return _nameMap.get(key);
		else
			return null;
	}
	
	public Integer registerID(String name){
		return getKey(name);
	}

	
	public Integer getID(String name) {
		if (_keyMap.containsKey(name))
			return _keyMap.get(name);
		else 
			return null;
	}
	
	
	@Override
	public String toString(){
		String ret = "SmaliEncoding nameMap:";

		for(Integer key : _nameMap.keySet()){
			ret += "--->" + key + " : " + _nameMap.get(key) + "\n";
		}		return ret;
	}

}
