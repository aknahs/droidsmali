package com.aknahs.smali.representations;

import com.google.gson.annotations.Expose;

public class SmaliLinkRepresentation extends SmaliRepresentation{
	@Expose private Integer _linkClass;
	@Expose private Integer _linkMethod;
		
	public SmaliLinkRepresentation(SmaliNameEncoding nameSchema, Integer cls, Integer mth){
		super(RepresentationType.LINK);
		_linkClass = cls;
		_linkMethod = mth;
		setNameEncoding(nameSchema);
	}	
	
	public SmaliLinkRepresentation(SmaliNameEncoding nameSchema, String cls, String mth){
		super(RepresentationType.LINK);
		setNameEncoding(nameSchema);
		_linkClass = getNameEncoding().registerID(cls);
		_linkMethod = getNameEncoding().registerID(mth);
	}
	
	public Integer getLinkMethod(){
		return _linkMethod;
	}
	
	public Integer getLinkClass(){
		return _linkClass;
	}
	
	public String getLinkName(){
		return getNameEncoding().getName(_linkMethod);
	}
	
	@Override
	public boolean equals(Object object){
		boolean ret = false;
		
		if(object != null && object instanceof SmaliLinkRepresentation){
			SmaliLinkRepresentation lnk = (SmaliLinkRepresentation) object;
			ret = (_linkClass == lnk.getLinkClass() && _linkMethod == lnk.getLinkMethod());
		}
		
		return ret;
	}
	
	@Override
	public int hashCode(){
		int cls = (int) _linkClass;
		int mth = (int) _linkMethod;
		
		int hash = cls;
		hash = 37*hash + mth;
		
		return hash;
	}
}
