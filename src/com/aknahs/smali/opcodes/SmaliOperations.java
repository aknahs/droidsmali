package com.aknahs.smali.opcodes;

public class SmaliOperations {
	
	public static enum INSTRUCTIONS {
		UNKNOWN, NEW_INSTANCE, INVOKE, SET_FIELD, READ_FIELD, RETURN
	}
	
	/*
	 * Instructions : http://pallergabor.uw.hu/androidblog/dalvik_opcodes.html
	 * */

	public static INSTRUCTIONS convertOperation(String op) {
		
		if(op.startsWith("invoke"))
			return INSTRUCTIONS.INVOKE;
		
		if(op.startsWith("new-instance"))
			return INSTRUCTIONS.NEW_INSTANCE;
		
		if(op.startsWith("iput"))
			return INSTRUCTIONS.SET_FIELD;
		
		if(op.startsWith("iget"))
			return INSTRUCTIONS.READ_FIELD;
		
		if(op.startsWith("return"))
			return INSTRUCTIONS.RETURN;
		
		return INSTRUCTIONS.UNKNOWN;
	}
}
