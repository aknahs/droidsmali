package com.aknahs.smali.opcodes;

public class SmaliDirectives {
	
	public static enum DIRECTIVES {
		UNKNOWN, CLASS, METHOD, FIELD, ANNOTATION, SUPER, END, LOCALS
	}
	
	public static boolean isDirective(String dir){
		dir = dir.trim();
		if(dir!=null && dir.length()>0 && dir.charAt(0) == '.')
			return true;
		return false;
	}
	
	/* From Jasmin syntax:
	 * 
	 * .catch .class .end .field .implements .interface .limit .line 
	 * .method .source .super .throws .var
	 * 
	 */
	public static DIRECTIVES convertDirective(String dir) {
		dir = dir.trim();
		switch (dir) {
		case ".class":
			return DIRECTIVES.CLASS;
		case ".locals":
			return DIRECTIVES.LOCALS;
		case ".method":
			return DIRECTIVES.METHOD;
		case ".end":
			return DIRECTIVES.END;
		case ".field":
			return DIRECTIVES.FIELD;
		case ".super":
			return DIRECTIVES.SUPER;
		case ".annotation":
			return DIRECTIVES.ANNOTATION;
		default:
			return DIRECTIVES.UNKNOWN;
		}
	}
}
