package com.aknahs.exporters;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import com.aknahs.log.SmaliLogger;
import com.aknahs.smali.representations.SmaliApkRepresentation;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class SmaliApkExporter {

	public static void generateJSON(String name, SmaliApkRepresentation apk) {
		try {
			System.out.print("SmaliApkExporter : " + "Generating json file(" + name
					+ ".json" + ")...");
			PrintWriter writer = new PrintWriter(
					name + ".json", "UTF-8");
			Gson gson = new GsonBuilder()
					.excludeFieldsWithoutExposeAnnotation()
					.setPrettyPrinting()
					.create();
			String json = gson.toJson(apk);
			writer.println(json);
			writer.close();
			SmaliLogger.log("SmaliApkExporter","done!");

		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static SmaliApkRepresentation loadJSON(String apkfile) {
		File file = new File(apkfile);
		return loadJSON(file);
	}
	
	//"apks/" + apkfile
	public static SmaliApkRepresentation loadJSON(File file) {

		if (file != null && !file.isDirectory()
				&& file.getName().contains(".json")) {
			BufferedReader br;
			try {
				br = new BufferedReader(new FileReader(file.getAbsolutePath()));

				Gson gson = new Gson();

				// convert the json string back to object
				SmaliApkRepresentation apk = gson.fromJson(br,
						SmaliApkRepresentation.class);

				SmaliLogger.log("SmaliApkExporter","Loaded json : " + apk.getApkName()
						+ ".json");

				SmaliLogger.log("SmaliApkExporter","Apk name - " + apk.getApkName());
				SmaliLogger.log("SmaliApkExporter","Number of classes - "
						+ apk.getClasses().size());
				SmaliLogger.log("SmaliApkExporter","Naming table - "
						+ apk.getNameEncoding().getNameMap().size());

				SmaliLogger.log("SmaliApkExporter","Apk main Activity - "
						+ apk.getManifest()._mainActivity);

				apk.revive();
				
				br.close();

				return apk;

			} catch (FileNotFoundException e) {
				System.out
						.println("There were no json files (folder or file missing?)");
			} catch (IOException e) {

				e.printStackTrace();
			}
		}

		return null;
	}
}
